$(function(){
    register.init();
});

var register = {};

register.init = function () {
    var classes = [];
    var total = 0;
    var isDirty = false;

    function addClass(event){
        var obj = {name:event.target.nextSibling.data,id:event.target.value};

        if(!_.where(classes,{id:obj.id}).length){
            classes.push(obj);
            isDirty = true;
        } else {
            classes.splice(_.findIndex(classes,{id:obj.id}),1);
            if(classes.length == 0){
                isDirty = false;
            }
        }

        buildTotal();
    }

    function buildTotal(){
        total = 0;
        $('#dsp_total').empty();
        $('#classList').empty();

        if(classes.length > 0){
            $('#noclass').hide();
        } else {
            $('#noclass').show();
        }

        _.each(classes,function (data) {

            $('#classList').append(data.name+'<br/>');

            if(total === 0){
                total = 45;
            } else {
                total += 30;
            }
        });

        $('#total').val(1);
        $('#dsp_total').append('$'+total);
    }

    this.AddClass = addClass;
    this.classes = classes;
    this.dirty = isDirty;
};