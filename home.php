<div class="container-fluid">
    <?php include_once('nav.html'); ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <img src="images/logo.png" alt="" class="img-responsive">

            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <h4 for="rcamerica" class="title">Presented By:
                    <img id="rcamerica" src="images/rcamerica.png" alt="">
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6" style="padding-top: 10px;">
                <img src="images/date.png" alt=""> <br>

            </div>

            <div class="col-sm-6 col-md-6 col-lg-6" style="padding-top: 10px;">
                <img src="images/classes.png" alt=""><br>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <img src="images/banana-logo.png" alt="" style="padding-top: 5px;">
                <br><br>
                <p class="lead">
                    Banana RC Raceway <br>
                    521 E Campbell Road #300 <br>
                    Richardson Tx, 75080 <br>
                    <a href="https://www.google.com/maps/place/Raceway+Banana+RC/@32.9756431,-96.7306453,16z/data=!4m8!1m2!2m1!1s521+E+Campbell+Road+%23300++Richardson+Tx,+75080!3m4!1s0x0:0x7b83d48162062160!8m2!3d32.9763187!4d-96.7381905" target="_blank">Map</a>
                </p>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="lead">
                    17.5 Blinky Sedan <br>
                    Mod Sedan <br>
                    USGT <br>
                    17.5 1/12th <br>
                    Mod 1/12th <br>
                    25.5 F1 <br>
                    Tamiya Mini
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6" style="padding-top: 10px;">
                <img src="images/schedule.png" alt=""> <br>

            </div>

            <div class="col-sm-6 col-md-6 col-lg-6" style="padding-top: 10px;">
                <img src="images/rules.png" alt=""><br>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="lead">
                    <strong>August 6th</strong><br>
                    8am -6am practice <br>
                    <strong>August 13th</strong><br>
                    7am -10:30am Practice <br>
                    10:30am -10:45am Drivers Meeting <br>
                    10:45am on Qualifiers 1,2,3 <br>
                    B Main chump bump! Making the A the hard way!
                    <strong>August 14th</strong><br>
                    7am -9am Practice <br>
                    9:15am Triple A, B, C Mains, etc
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="lead">
                    Rocket Round <br><br>
                    ROAR Rules unless otherwise posted. This includes, but is not limited to: <br>
                    <ol>
                        <li>6.5mm max rotor for spec classes</li>
                        <li>Blinky Mode</li>
                        <li>4.2v Per Cell</li>
                        <li>
                            3 Sets of spec tires per class <br>
                            Touring Car Spec Tire: Gravity G-Spec <br>
                            Spec Sauce SXT 3.0
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-24 col-md-24 col-lg-24">
                <img src="images/allsponsors.png" alt="">
            </div>
        </div>
    </div>
</div>
