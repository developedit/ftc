<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Full Throttle Champs</title>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
</head>
<body>

<?php include_once('home.php'); ?>

  <script src="assets/js/underscore.js"></script>
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>

</body>
</html>
