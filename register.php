<!doctype html>
<?php include_once('connection/connection.php'); ?>
<html>
<head>
    <meta charset="utf-8">
    <title>Full Throttle Champs</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
</head>
<body>
<div class="container-fluid">
    <?php include_once('nav.html'); ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-7 col-md-7 col-lg-7">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Racer Information
                    </div>
                    <div class="panel-body">
                        <form action="payment.php" method="post">
                            <div class="form-group">
                                <label for="firstName">Full Name</label>
                                <input type="text" class="form-control" id="racerName" name="racerName" placeholder="First Name" required>
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="tel" class="form-control" id="racerPhone" name="racerPhone" placeholder="Phone Number">
                            </div>
                            <div class="form-group">
                                <label for="email">Email Address</label>
                                <input type="email" class="form-control" id="racerEmail" name="racerEmail" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Select Class(es)</label>
                                <div class="checkbox">
                                    <label>
                                        <?php include_once('getclasses.php'); ?>
                                    </label>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success">Submit</button>
                            <input type="hidden" name="cmd" value="_xclick" />
                            <input type="hidden" name="no_note" value="1" />
                            <input type="hidden" name="lc" value="US" />
                            <input type="hidden" name="currency_code" value="USD" />
                            <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />
                            <input type="hidden" name="first_name" value=""  />
                            <input type="hidden" name="last_name" value=""  />
                            <input type="hidden" name="payer_email" value=""  />
                            <input type="hidden" name="item_number" value="100" / >
                            <input type="hidden" name="total" id="total" value=""/>
                            <input type="hidden" name="racerId" id="racerId"/>
                        </form>
                    </div>

                </div>
            </div>
            <div class="col-sm-5 col-md-5 col-lg-5">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Signed Up For
                    </div>
                    <div class="panel-body">
                       <span id="noclass">No Classes Selected</span>
                        <div id="classList"></div>
                    </div>
                    <div class="panel-footer">
                        Total: <span id="dsp_total">$0</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="assets/js/underscore.js"></script>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="js/register.js"></script>
</body>
</html>
