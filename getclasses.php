<?php
$sql = "SELECT classId,className FROM raceClasses ";
$stmt = sqlsrv_query( $conn, $sql);

if( $stmt === false ) {
    die( print_r( sqlsrv_errors(), true));
}

while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
    echo '<input type="checkbox" name="raceClass[]" value="'.$row['classId'].'" onclick="register.AddClass(event)">'.$row['className'].'<br />';
}

sqlsrv_free_stmt( $stmt);